import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,Image,TextInput,Alert
} from 'react-native';
import { Card, CardItem, Body,Button } from 'native-base'
import { ScrollView, TouchableOpacity } from 'react-native-gesture-handler';


export default class SignupFifth extends Component {
    static navigationOptions={
        header:null
    }
    state = {
      username: '',
      password: ''
       }

       
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
      <ScrollView>
      <View style={styles.firstrow}>
<ImageBackground  source={require('./images/home.png')}
                        style={{width: '100%',height:200,}}
                        imageStyle={{resizeMode:'stretch'}}>       


            <Image  source={require('./images/imgupload.png')}
                                    style={{width:100,height:100,marginLeft:220,marginTop:40 }}/>

                         </ImageBackground>

     
      <View style={styles.thirdrow}>
      <Text style={{fontSize:30,fontWeight:'bold',marginLeft:10}}>Register</Text>
        <Text style={{fontSize:10,marginLeft:10}}>Kindly provide required information & follow the steps!</Text>

      </View>

      </View>
      
      

      <View style={styles.fourthrow}>

      <Card style={{marginLeft:0,height:150,width:270,paddingTop:20,paddingLeft:20,paddingRight:20,borderBottomEndRadius:70,borderTopEndRadius:70,elevation:10}}>
            <CardItem>
              <Body >

<TextInput   underlineColorAndroid="transparent"
              placeholder="Password" 
              placeholderTextColor="black" 
              maxLength={20}  
              style={{color:'black',fontSize:15,alignItems:'center',fontWeight:'bold',width:200}}
              value ={this.state.username}
               onChangeText = {text => this.setState({username:text})}></TextInput>

                <Text style={{marginBottom:10}} numberOfLines={1}>
                    ____________________________
                  </Text>
             <TextInput placeholder="Confirm Password" placeholderTextColor="black" secureTextEntry={true} maxLength={10} 
             style={{color:'black',fontSize:15,alignItems:'center',fontWeight:'bold'}}
             value ={this.state.password}
             onChangeText = {text => this.setState({password:text})}></TextInput>
             </Body>
            </CardItem>
          </Card>

      </View>
                   

                <Image  source={require('./images/tick.png')}
                                    style={{width:70,height:70,transform: [{'translate':[240,-145,1]}] }}/>
      {/* <View style={styles.sixrow}>
      <ImageBackground  source={require('./images/button.png')}
                        style={{width: '60%',height:100,}}
                        imageStyle={{resizeMode:'stretch'}}>
      </ImageBackground>
      </View> */}
      <View style={styles.sevenrow}>
      <ImageBackground  source={require('./images/footer.png')}
                        style={{width: '100%',height:200,}}
                        imageStyle={{resizeMode:'stretch'}}>


<ImageBackground  source={require('./images/button.png')}
                        style={{width: '65%',height:100,}}
                        imageStyle={{resizeMode:'stretch'}}
                         >
                        <Text style={{paddingTop:35,paddingLeft:10,color:'white'}} onPress={() => navigate('Profile')} >REGISTER</Text>
      </ImageBackground>

      </ImageBackground>
      </View>



</ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  firstrow: {
    flex: 1,
    backgroundColor: "white"
  },

  secondrow: {
    flex: .4,
    backgroundColor: "white",
    flexDirection:'row',
    marginLeft:180,
    marginTop:100

  },

  thirdrow: {
    flex: .4,
    backgroundColor: "white",
    marginTop:20
    
  },

  fourthrow: {
    flex: 1,
    flexDirection:'row',
    backgroundColor: "white",
    marginTop:20
  },
  
  fifthrow: {
    flex: .2,
    backgroundColor: "white"
  }
  ,
  sixrow: {
    flex: .4,
    backgroundColor: 'white'
  },
  sevenrow: {
    flex: 1,
    backgroundColor: 'white',
    height:200
  }
  
});