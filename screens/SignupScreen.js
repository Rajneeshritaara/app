import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,Image,TextInput
} from 'react-native';
import { Card, CardItem, Body,Button } from 'native-base'
import { ScrollView } from 'react-native-gesture-handler';


export default class SignupScreen extends Component {
    static navigationOptions={
        header:null
    }
  
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
      <ScrollView>
      <View style={styles.firstrow}>
<ImageBackground  source={require('./images/home.png')}
                        style={{width: '100%',height:200,}}
                        imageStyle={{resizeMode:'stretch'}}>       


            <Image  source={require('./images/imgupload.png')}
                                    style={{width:100,height:100,marginLeft:220,marginTop:40 }}/>

                         </ImageBackground>
      <View style={styles.thirdrow}>
      <Text style={{fontSize:30,fontWeight:'bold',marginLeft:10}}>Register</Text>
        <Text style={{fontSize:10,marginLeft:10}}>Kindly provide required information & follow the steps!</Text>

      </View>

      </View>
      
      

      <View style={styles.fourthrow}>

      <Card style={{marginLeft:0,height:200,width:285,paddingLeft:5,borderBottomEndRadius:50,borderTopEndRadius:50,borderWidth:1,borderColor:'transparent',elevation:10}}>
            <CardItem style={{borderColor:'transparent',borderWidth:1,width:250,height:150,marginTop:10,marginRight:5}}>
              <Body >
                <TextInput placeholder="Username" 
                placeholderTextColor="black" 
                maxLength={20}
                 autoCorrect={false}
                 style={{color:'black',fontSize:15,fontWeight:'bold',width:200}}></TextInput>

                <Text style={{marginBottom:10}} numberOfLines={1}>
                    ____________________________
                  </Text>
             <TextInput placeholder="Password"  placeholderTextColor="black" secureTextEntry={true} maxLength={10} style={{color:'black',fontSize:15,fontWeight:'bold'}}></TextInput>
             <Text style={{marginBottom:10}} numberOfLines={1}>
                    ____________________________
                  </Text>
                  <TextInput placeholder="Mobile Number" autoCorrect={false} placeholderTextColor="black" maxLength={11} style={{color:'black',fontSize:15,fontWeight:'bold',width:200}}></TextInput>
            
             </Body>
            </CardItem>
          </Card>
          
                         
                                    
      </View>

                <Image  source={require('./images/goicon.png')}
                                    style={{width:70,height:70,transform: [{'translate':[245,-155,1]}] }}/>

  
      {/* <View style={styles.sixrow}>
      <ImageBackground  source={require('./images/button.png')}
                        style={{width: '60%',height:100,}}
                        imageStyle={{resizeMode:'stretch'}}>
      </ImageBackground>
      </View> */}
      <View style={styles.sevenrow}>
      <ImageBackground  source={require('./images/footer.png')}
                        style={{width: '100%',height:200,}}
                        imageStyle={{resizeMode:'stretch'}}>


<ImageBackground  source={require('./images/button.png')}
                        style={{width: '65%',height:100,}}
                        imageStyle={{resizeMode:'stretch'}}>
                        <Text style={{paddingTop:35,paddingLeft:10,color:'white'}} onPress={() => navigate('SignUp2')}>Back to LOGIN</Text>
      </ImageBackground>

      </ImageBackground>
      </View>



</ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  firstrow: {
    flex: .5,
    backgroundColor: "white"
  },

  secondrow: {
    flex: .3,
    backgroundColor: "white",
    flexDirection:'row',
    marginLeft:180,
    marginTop:100

  },

  thirdrow: {
    flex: .3,
    backgroundColor: "white",
   
    
  },

  fourthrow: {
    flex: .5,
    flexDirection:'row',
    backgroundColor: "white",
    marginTop:25,
    borderColor:'transparent',
    borderWidth:1
  },
  
  fifthrow: {
    flex: .2,
    backgroundColor: "white"
  }
  ,
  sixrow: {
    flex: .4,
    backgroundColor: 'white'
  },
  sevenrow: {
    flex: 1,
    backgroundColor: 'white',
    height:200
  }
  
});