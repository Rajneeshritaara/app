import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,ImageBackground,Image,TextInput,Picker
} from 'react-native';
import { Dropdown } from 'react-native-material-dropdown';
import { Card, CardItem, Body,Button } from 'native-base'
import { ScrollView } from 'react-native-gesture-handler';

let data = [{
    value: 'UP'
  }, {
    value: 'MP',
  }, {
    value: 'HP',
  }];

export default class SignupFourth extends Component {
    
    static navigationOptions={
        header:null
    }
    state={
        language:'',
        branch:''
    }
  
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={styles.container}>
      <ScrollView>
      <View style={styles.firstrow}>
<ImageBackground  source={require('./images/home.png')}
                        style={{width: '100%',height:200,}}
                        imageStyle={{resizeMode:'stretch'}}>       


            <Image  source={require('./images/imgupload.png')}
                                    style={{width:100,height:100,marginLeft:220,marginTop:40 }}/>

                         </ImageBackground>
      <View style={styles.thirdrow}>
      <Text style={{fontSize:30,fontWeight:'bold',marginLeft:10}}>Register</Text>
        <Text style={{fontSize:10,marginLeft:10}}>Kindly provide required information & follow the steps!</Text>

      </View>

      </View>
      
      

      <View style={styles.fourthrow}>

      <Card style={{marginLeft:0,height:200,width:285,paddingLeft:5,borderBottomEndRadius:50,borderTopEndRadius:50,borderWidth:1,borderColor:'transparent',elevation:10}}>
            <CardItem style={{borderColor:'transparent',borderWidth:1,width:250,height:150,marginTop:10,marginRight:5}}>
              <Body >
                <TextInput placeholder="About" autoCorrect={false} maxLength={15} placeholderTextColor="black"  style={{color:'black',fontSize:15,fontWeight:'bold',marginLeft:3}}></TextInput>

                <Text numberOfLines={1}>
                    ____________________________
                  </Text>
            
                  <View style={{ flexDirection:'row',width:'100%',justifyContent:"space-between",}}>
        
               
                  {/* <Text style={{fontWeight:"bold",padding:5,fontSize:15}}>state</Text> */}
                  
                  <Picker
  selectedValue={this.state.branch}
  style={{height: 50, width: 200}}
  onValueChange={(itemValue, itemIndex) =>
    this.setState({language: itemValue})
  }>
  <Picker.Item label="Shirt Size" value="Branch" fontWeight="bold" />
  <Picker.Item label="Any" value="js" />
  <Picker.Item label="Any" value="js" />
  <Picker.Item label="Any" value="js" />
</Picker>
                
                  
                </View>
                <Text numberOfLines={1}>
                    ____________________________
                  </Text>
<View style={{marginTop:5}}>
<TextInput placeholder="Interest/Hobbies" autoCorrect={false} maxLength={15} placeholderTextColor="black"  style={{color:'black',fontSize:15,fontWeight:'bold',marginLeft:3}}></TextInput>
              
</View>      
             </Body>

            </CardItem>
          </Card>

      </View>

                <Image  source={require('./images/goicon.png')}
                                    style={{width:70,height:70,transform: [{'translate':[245,-155,1]}] }}/>

  
      {/* <View style={styles.sixrow}>
      <ImageBackground  source={require('./images/button.png')}
                        style={{width: '60%',height:100,}}
                        imageStyle={{resizeMode:'stretch'}}>
      </ImageBackground>
      </View> */}
      <View style={styles.sevenrow}>
      <ImageBackground  source={require('./images/footer.png')}
                        style={{width: '100%',height:200,}}
                        imageStyle={{resizeMode:'stretch'}}>


<ImageBackground  source={require('./images/button.png')}
                        style={{width: '65%',height:100,}}
                        imageStyle={{resizeMode:'stretch'}}>
                        <Text style={{paddingTop:35,paddingLeft:10,color:'white'}} onPress={() => navigate('SignUp5')}>Back to LOGIN</Text>
      </ImageBackground>

      </ImageBackground>
      </View>



</ScrollView>

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },

  firstrow: {
    flex: .5,
    backgroundColor: "white"
  },

  secondrow: {
    flex: .3,
    backgroundColor: "white",
    flexDirection:'row',
    marginLeft:180,
    marginTop:100

  },

  thirdrow: {
    flex: .3,
    backgroundColor: "white",
   
    
  },

  fourthrow: {
    flex: .5,
    flexDirection:'row',
    backgroundColor: "white",
    marginTop:25,
    borderColor:'transparent',
    borderWidth:1
  },
  
  fifthrow: {
    flex: .2,
    backgroundColor: "white"
  }
  ,
  sixrow: {
    flex: .4,
    backgroundColor: 'white'
  },
  sevenrow: {
    flex: 1,
    backgroundColor: 'white',
    height:200
  }
  
});