import React, { Component } from 'react'
import {View,Text,ImageBackground,TextInput,Image,TouchableOpacity,StyleSheet} from 'react-native';
import AntDesign  from 'react-native-vector-icons/AntDesign';
import { ScrollView } from 'react-native-gesture-handler';
export default class ChangePwd extends Component {
    static navigationOptions={
        header:null
    }
  render() {
    const {navigate} = this.props.navigation;
    return (
      <View>
      <ScrollView>
         <ImageBackground  source={require('./images/home.png')}
                                    style={{height:200,}}
                                    imageStyle={{resizeMode:'stretch'}}>
                                    <View style={{marginTop:70,borderColor:'transparent',borderWidth:1,marginLeft:8,width:220,height:100}}>
                                    <Text style={{color:'white',fontWeight:'bold',fontSize:20,paddingTop:10}}>Change Password </Text>
                                    </View>
                                    
                    </ImageBackground>

              <View style={{marginLeft:10,marginTop:10}}>
                <Text>Do you want to you change your password,</Text>
                <Text>follow the simple steps and click</Text>
               <Text>on the tick !</Text>

              </View>

              <ImageBackground  source={require('./images/change.png')}
                                    style={{ width:'90%',height:200,marginTop:15}}
                                    imageStyle={{resizeMode:'stretch'}}>
                                    <View style={{paddingLeft:15,paddingTop:15,marginTop:7}}>
<TextInput placeholder="Old Password" placeholderTextColor="black" autoCorrect={false} maxLength={15}  style={{color:'black',fontSize:15,alignItems:'center',fontWeight:'bold'}}></TextInput>

<Text style={{marginBottom:10}} numberOfLines={1}>
    ____________________________
  </Text>

  <TextInput placeholder="New Password" secureTextEntry={true} autoCorrect={false} placeholderTextColor="black" maxLength={20} style={{color:'black',fontSize:15,alignItems:'center',fontWeight:'bold'}}></TextInput>
<Text style={{marginBottom:10}} numberOfLines={1}>
    ____________________________
  </Text>
<TextInput placeholder="Re-enter Password" placeholderTextColor="black" maxLength={10} style={{color:'black',fontSize:15,alignItems:'center',fontWeight:'bold'}}></TextInput>

</View>                       
                    </ImageBackground>

                    <Image  source={require('./images/tick.png')}
                                    style={{width:60,height:60,transform: [{'translate':[240,-135,1]}] }}/>


                    <View>
  
  <TouchableOpacity style={{ flexDirection:'row',width:100,height:52,borderWidth:1,borderColor:'transparent',marginLeft:100,marginBottom:20}}
  onPress={() => navigate('Test')} >
  
<AntDesign name="arrowleft" size={15} color="black" />
<View>
    <Text style={{fontSize:10}} > Back  to Setting</Text> 
            </View>
  
     </TouchableOpacity>
</View>
<ImageBackground  source={require('./images/logo.png')}
                                    style={{width:180,height:45,marginLeft:145,transform:[{translateY:-30}]}}
                                    imageStyle={{resizeMode:'stretch'}}>
                                    
                    </ImageBackground>

                    </ScrollView>
      </View>
    )
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    width:'100%',
    height:'100%',
    backgroundColor: '#fff',
  },
  gradient:{
    padding: 15,
     borderRadius: 5 ,
     width:150,
     borderTopRightRadius:25,
     borderBottomRightRadius:25,
     marginLeft:0,
     paddingLeft:0
  },


});