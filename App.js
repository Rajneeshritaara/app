import React from 'react';
import {AppRegistry} from 'react-native'
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';

import SignUpScreen from './screens/SignupScreen';
import ProfilePage from './screens/ProfilePage';
import RouteScreen from './screens/RouteScreen';
import ChangePwd from './screens/ChangePwd';
import Test from './screens/Test';
import SignupSecond from './screens/SignupSecond';
import SignupThird from './screens/SignupThird';
import SignupFourth from './screens/SignupFourth';
import SignupFifth from './screens/SignupFifth';


console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

export default class App extends React.Component{

    // componentDidMount() {
    //     if (!EventEmitter.listeners('myEvent').length) {
    //         EventEmitter.addListener('myEvent', this.handleMyEvent);
    //     }
    // }
    
    render(){
        return <AppContainer/>
    }
}

const MainNavigator = createStackNavigator({
Route:{screen:RouteScreen},
SignUp: {screen: SignUpScreen},
SignUp2:{screen:SignupSecond},
SignUp3:{screen:SignupThird},
SignUp4 :{screen:SignupFourth},
SignUp5:{screen:SignupFifth},
Profile:{screen:ProfilePage},
Change:{screen:ChangePwd},
Test :{screen:Test}
});

const AppContainer = createAppContainer(MainNavigator);


AppRegistry.registerComponent('App', () => App);
